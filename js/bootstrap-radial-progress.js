(function($){
 	$.fn.extend({
		percentcircle: function(options) {
			var defaults = {
		    animate: true,
				bgColor: '#182a36',
				fillColor: '#cc5e43',
			};

			var that = this,
				template = '<div class="radial-wrap"><div class="radial-circle"><div class="radial-inner-circle"><span class="radial-percentage">{{percentage}}</span></div></div></div>',					
				options =  $.extend(defaults, options)					

			function init(){
				that.each(function(){
					var $this = $(this),
						parentWidth = $(this).parent().width(),
						diameter = parentWidth,
						guage = Math.round(parentWidth / 12.8),
						percentSize = Math.round(parentWidth / 3.9) + 'px',
						styles = {
					    cirContainer : {
							  'width':diameter,
								'height':diameter
							},
							cir : {
						    'width': diameter,
						    'height': diameter,
						    'background-color': defaults.fillColor,
						    'background-image' : 'linear-gradient(91deg, transparent 50%, '+defaults.bgColor+' 50%), linear-gradient(90deg, '+defaults.bgColor+' 50%, transparent 50%)'
							},
							cirCover: {
						    'top': guage,
						    'left': guage,
						    'width': diameter - (guage * 2),
						    'height': diameter - (guage * 2)
							},
							percent: {
								'width': diameter - (guage * 2),
						    'height': diameter - (guage * 2),
						    'line-height': diameter - (guage * 2) + 'px',
						    'font-size': percentSize
				      }
						},
						now = Math.round($this.attr('aria-valuenow')),
						min = Math.round($this.attr('aria-valuemin')),
						max = Math.round($this.attr('aria-valuemax')),
						deg = now * (360 / (max-min)),
						perc = Math.round(now / (max-min) * 100),
						stop = options.animate ? 0 : deg,
						$chart = $(template.replace('{{percentage}}',perc+'%'));

					$('.radial-wrap',$chart).css(styles.cirContainer);
					$('.radial-circle', $chart).css(styles.cir);
					$('.radial-inner-circle', $chart).css(styles.cirCover);
					$('.radial-percentage', $chart).css(styles.percent);
					$this.append($chart);

					setTimeout(function(){
						animateChart(deg,parseInt(stop),$('.radial-circle', $chart));
					},250)
 	    	});
			}

			var animateChart = function (stop,curr,$elm){
				var deg = curr;
				if(curr <= stop){
					if (deg<=180){
						$elm.css('background-image','linear-gradient(' + (90+deg) + 'deg, transparent 50%, '+options.bgColor+' 50%),linear-gradient(90deg, '+options.bgColor+' 50%, transparent 50%)');
	      	}else{
		  		  $elm.css('background-image','linear-gradient(' + (deg-90) + 'deg, transparent 50%, '+options.fillColor+' 50%),linear-gradient(90deg, '+options.bgColor+' 50%, transparent 50%)');
			    }
					curr ++;
					setTimeout(function(){
						animateChart(stop,curr,$elm);
					},1);
				}
			};			
			
			init();
   	}
	});
	
})(jQuery);

$('.radial-progress').percentcircle();