# Bootstrap Radial Progress

Bootstrap Radial Progress is a jQuery plugin based on the [jQuery-Circle-Charts] plugin by [jamiepickett].

## Version
1.0

## Usage
Include the CSS and JS files and add a radial progressbar:

```html
<div class="radial-progress" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100"></div>
```

License
----

MIT
[jQuery-Circle-Charts]:https://github.com/jamiepickett/jQuery-Circle-Charts
[jamiepickett]:https://github.com/jamiepickett
